import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//react-bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
  document.getElementById('root')
  );


// -----
/*ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);*/

/*const name = "John Smith"
const element = <h1>Hello, {name}</h1>

ReactDOM.render(
  element, document.getElementById('root')
);*/

//Mini Activity
/*

Create another variable that is an object which contains properties firstName and lastName

Using a function, join the firstName and lastNam and display it in the browser*/

/*const lover = { firstName: 'Ralph', lastName: 'Sanchez'};

function fullName(lover){
  return `${lover.firstName} ${lover.lastName}`
}

const element = <h1>I love you, { fullName(lover) }</h1>

ReactDOM.render(
  element, document.getElementById('root'))
*/
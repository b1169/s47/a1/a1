
import {Row, Col, Container, Button, Form} from "react-bootstrap";
import { useState, useEffect } from 'react';
function Login(){


	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	function loginUser(e){
		e.preventDefault()

			setEmail("")
			setPassword("")


			alert("You are now logged in")

	}

	useEffect ( () => {

		if(email !== "" && password !==""){
			setIsDisabled(false)
		}
	}, [email, password])

	return(

		<Container className="my-5">
		<Row className="justify-content-center">
		<Col xs= {10} md={6}>

				<Form onSubmit={(e) => 
					loginUser(e)}>
			{/*Email address*/}
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control type="email" placeholder="Enter email" 
				    	value={email} 
				    	onChange={ (e) => {
				    		setEmail(e.target.value)
				    	}}
				    />
				   
				  </Form.Group>

			{/*Password*/}

				  <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" placeholder="Password" 

						value={password}
				    	onChange={ (e) => {
				    		setPassword(e.target.value)
				    	}}			

				    />
				  </Form.Group>
				  
			{/*Submit Button*/}

				  <Button variant="primary" type="submit" disabled={isDisabled}>
				    Login
				  </Button>
				</Form>

		</Col>
		</Row>
		</Container>

	)
}
export default Login;
import {Card,Row, Col, Container} from "react-bootstrap";

function Highlights(){

	return(

		<Container fluid className = "mb-4">
			<Row>
				<Col xs = {12} md = {4}>

				<Card className="cardHighlights p-3">
			
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>

				</Col>

				<Col xs = {12} md = {4}>

				<Card className="cardHighlights p-3">
			
				  <Card.Body>
				    <Card.Title>Study Now, Pay Later</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>

				</Col>

				<Col xs = {12} md = {4}>

				<Card className="cardHighlights p-3" >
			
				  <Card.Body>
				    <Card.Title>Be Part Of Our Community</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>

				</Col>

			</Row>
		</Container>
		)

}


export default Highlights;